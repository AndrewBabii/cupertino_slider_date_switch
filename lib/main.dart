import 'package:flutter/cupertino.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void doNothing(DateTime date) {}

  bool _value = true;
  double cupSlid = 0.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: CupertinoPageScaffold(
        child: Column(
          children: [
            Container(
              height: 220,
              child: CupertinoDatePicker(
                onDateTimeChanged: doNothing,
              ),
            ),
            CupertinoSwitch(
              value: _value,
              onChanged: (bool value) {
                setState(() {
                  _value = value;
                });
              },
            ),
            CupertinoSlider(
                value: cupSlid,
                onChanged: (slid) {
                  setState(() {
                    cupSlid = slid;
                  });
                })
          ],
        ),
      ),
    );
  }
}
